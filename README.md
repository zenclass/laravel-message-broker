You can install this package using composer:
```bash
composer require zenclass/laravel-message-broker
```

Then you need to publish the configuration file using
```bash
php artisan vendor:publish --provider="Zenclass\MessageBroker\MessageBrokerServiceProvider"
```
