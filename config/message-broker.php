<?php

return [
    'producer' => [
        /*
        |--------------------------------------------------------------------------
        | Default Message Broker Producer Driver
        |--------------------------------------------------------------------------
        |
        | The name specified in this option should match one of the drivers
        | defined in the "drivers" section.
        |
        */
        'default' => env('MESSAGE_BROKER_PRODUCER_DRIVER', 'log'),

        /*
        |--------------------------------------------------------------------------
        | Message Broker Producer Drivers
        |--------------------------------------------------------------------------
        |
        | Here you may configure the message broker drivers for your application.
        |
        | Available Drivers: "log", "kafka-rest"
        |
        */
        'drivers' => [
            'kafka-rest' => [
                'host' => env('KAFKA_REST_HOST', 'localhost:8082'),
                'user' => env('KAFKA_REST_USER'),
                'password' => env('KAFKA_REST_PASS'),
            ],
        ],
    ],
];
