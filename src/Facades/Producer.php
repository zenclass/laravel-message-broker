<?php

namespace Zenclass\MessageBroker\Facades;

use Illuminate\Support\Facades\Facade;
use Zenclass\MessageBroker\Producer\Contracts\CanProduceMessageToTopic;

/**
 * @method static CanProduceMessageToTopic to(string $topic)
 * @method static CanProduceMessageToTopic message(mixed $message)
 * @method static void send()
 * @method static void sendBatch(array $messages)
 */

class Producer extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return CanProduceMessageToTopic::class;
    }
}
