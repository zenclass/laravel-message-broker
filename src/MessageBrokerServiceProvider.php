<?php

namespace Zenclass\MessageBroker;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;
use Zenclass\MessageBroker\Producer\Contracts\CanProduceMessageToTopic;
use Zenclass\MessageBroker\Producer\ProducerManager;

class MessageBrokerServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function boot(): void
    {
        $this->publishes([
            __DIR__ . "/../config/message-broker.php" => config_path('message-broker.php'),
        ]);
    }

    public function register(): void
    {
        $this->app->singleton(CanProduceMessageToTopic::class, fn ($app) => new ProducerManager($app));
    }

    public function provides(): array
    {
        return [CanProduceMessageToTopic::class];
    }
}
