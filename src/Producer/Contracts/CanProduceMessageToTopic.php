<?php

namespace Zenclass\MessageBroker\Producer\Contracts;

interface CanProduceMessageToTopic
{
    public function to(string $topic): static;

    public function message(mixed $message): static;

    public function send(): void;

    public function sendBatch(array $messages): void;
}
