<?php

namespace Zenclass\MessageBroker\Producer\Drivers;

use Exception;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

class KafkaRestProducer extends Producer
{
    private array $records;

    /**
     * @throws RequestException
     * @throws Exception
     */
    public function send(): void
    {
        $this->records = [
            ['value' => $this->message],
        ];

        $this->makeRequest();
    }

    /**
     * @throws RequestException
     * @throws Exception
     */
    public function sendBatch(array $messages): void
    {
        $this->records = array_map(
            fn ($message) => ['value' => $message],
            $messages
        );

        $this->makeRequest();
    }

    /**
     * @throws RequestException
     * @throws Exception
     */
    public function makeRequest(): void
    {
        $url = $this->getRequestUrl();
        $headers = $this->getRequestHeaders();
        $body = $this->getRequestBody();

        $request = Http::withHeaders($headers);

        list($user, $password) = $this->getBasicAuth();
        if ($user && $password) {
            $request->withBasicAuth($user, $password);
        }

        $response = $request->post($url, $body);

        $this->handleErrors($response);
    }

    private function getRequestUrl(): string
    {
        $host = config('message-broker.producer.drivers.kafka-rest.host');
        $topic = $this->topic;

        return "$host/topics/$topic";
    }

    private function getRequestHeaders(): array
    {
        return [
            'Content-Type' => 'application/vnd.kafka.json.v2+json',
            'Accept' => 'application/vnd.kafka.v2+json',
        ];
    }

    private function getRequestBody(): array
    {
        return [
            'records' => $this->records,
        ];
    }

    private function getBasicAuth(): array
    {
        return [
            config('message-broker.producer.drivers.kafka-rest.user'),
            config('message-broker.producer.drivers.kafka-rest.password')
        ];
    }

    /**
     * @throws RequestException
     * @throws Exception
     */
    private function handleErrors(Response $response): void
    {
        $this->handleRequestErrors($response);

        /**
         * Так как в Kafka Rest существует возможность отправки сразу нескольких записей,
         * для каждой из них возвращается собственный ответ в массиве.
         *
         * Если при отправке конкретной записи произошла ошибка, сам запрос ошибку не выбрасывает.
         */
        $this->handleRecordErrors($response);
    }

    /**
     * @throws RequestException
     */
    private function handleRequestErrors(Response $response): void
    {
        $response->throw();
    }

    /**
     * @throws Exception
     */
    private function handleRecordErrors(Response $response): void
    {
        $records = $response->json('offsets');

        foreach ($records as $record) {
            $errorCode = Arr::get($record, 'error_code');
            if (!$errorCode) {
                continue;
            }

            $errorMessage = Arr::get($record, 'error');

            throw new Exception($errorMessage, $errorCode);
        }
    }
}
