<?php

namespace Zenclass\MessageBroker\Producer\Drivers;

use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;

class LogProducer extends Producer
{
    public function send(): void
    {
        $logger = $this->getLogger();

        $logger->debug('Message sent to broker', [
            'topic' => $this->topic,
            'message' => $this->message,
        ]);
    }

    public function sendBatch(array $messages): void
    {
        $logger = $this->getLogger();

        foreach ($messages as $message) {
            $logger->debug('Message sent to broker', [
                'topic' => $this->topic,
                'message' => $message,
            ]);
        }
    }

    private function getLogger(): LoggerInterface
    {
        $config = [
            'driver' => 'single',
            'path' => storage_path('logs/laravel-message-broker.log'),
        ];

        return Log::build($config);
    }
}
