<?php

namespace Zenclass\MessageBroker\Producer\Drivers;

use Zenclass\MessageBroker\Producer\Contracts\CanProduceMessageToTopic;

abstract class Producer implements CanProduceMessageToTopic
{
    protected string $topic;
    protected mixed $message;

    public function to(string $topic): static
    {
        $this->topic = $topic;

        return $this;
    }

    public function message(mixed $message): static
    {
        $this->message = $message;

        return $this;
    }
}
