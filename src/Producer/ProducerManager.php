<?php

namespace Zenclass\MessageBroker\Producer;

use Zenclass\MessageBroker\Producer\Drivers\KafkaRestProducer;
use Zenclass\MessageBroker\Producer\Drivers\LogProducer;
use Illuminate\Support\Manager;

class ProducerManager extends Manager
{
    public function createKafkaRestDriver(): KafkaRestProducer
    {
        return new KafkaRestProducer();
    }

    public function createLogDriver(): LogProducer
    {
        return new LogProducer();
    }

    public function getDefaultDriver(): string
    {
        return $this->config->get('message-broker.producer.default', 'log');
    }
}
