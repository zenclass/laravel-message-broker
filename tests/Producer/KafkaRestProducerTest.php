<?php

namespace Zenclass\MessageBroker\Tests\Producer;

use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Http;
use Zenclass\MessageBroker\Facades\Producer;
use Zenclass\MessageBroker\Producer\Contracts\CanProduceMessageToTopic;
use Zenclass\MessageBroker\Tests\TestCase;

class KafkaRestProducerTest extends TestCase
{
    /**
     * @define-env usesKafkaRestProducerDriver
     */
    public function testThatKafkaRestProducerSendRequestToKafkaRestHost(): void
    {
        $topicName = 'test';
        $message = 'test message';

        Http::fake([
            '*' => Http::response([
                'offsets' => [
                    [],
                ]
            ])
        ]);

        Producer::to($topicName)
            ->message($message)
            ->send();

        Http::assertSent(function (Request $request) use ($topicName) {
            return $request->url() === "http://localhost:8082/topics/$topicName";
        });
    }

    /**
     * @define-env usesKafkaRestProducerDriver
     *
     * @dataProvider regularExceptionCodesToBeSimplyThrown
     *
     * @see https://docs.confluent.io/platform/current/kafka-rest/api.html#post--topics-(string-topic_name)
     */
    public function testThatKafkaRestProducerThrowsRegularExceptionCodes(int $errorCode): void
    {
        $topicName = 'test';
        $message = app(CanProduceMessageToTopic::class);

        Http::fake([
            '*' => Http::response(status: $errorCode),
        ]);

        $this->expectExceptionCode($errorCode);

        Producer::to($topicName)
            ->message($message)
            ->send();
    }

    private function regularExceptionCodesToBeSimplyThrown(): array
    {
        return [
            '404' => ['errorCode' => 404],
            '408' => ['errorCode' => 408],
            '422' => ['errorCode' => 422],
        ];
    }

    /**
     * @define-env usesKafkaRestProducerDriver
     *
     * @dataProvider messageExceptionCodesToBeThrown
     *
     * @see https://docs.confluent.io/platform/current/kafka-rest/api.html#post--topics-(string-topic_name)
     */
    public function testThatKafkaRestProducerThrowsMessageExceptionOnSuccessfulResponse(int $errorCode): void
    {
        $topicName = 'test';
        $message = app(CanProduceMessageToTopic::class);

        Http::fake([
            '*' => Http::response([
                'offsets' => [
                    [
                        'error_code' => $errorCode
                    ],
                ]
            ]),
        ]);

        $this->expectExceptionCode($errorCode);

        Producer::to($topicName)
            ->message($message)
            ->send();
    }

    private function messageExceptionCodesToBeThrown(): array
    {
        return [
            '400' => ['errorCode' => 40000],
            '500' => ['errorCode' => 50000],
            'any' => ['errorCode' => 200],
        ];
    }
}
