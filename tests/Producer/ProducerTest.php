<?php

namespace Zenclass\MessageBroker\Tests\Producer;

use Illuminate\Contracts\Container\BindingResolutionException;
use Zenclass\MessageBroker\Producer\Contracts\CanProduceMessageToTopic;
use Zenclass\MessageBroker\Producer\Drivers\KafkaRestProducer;
use Zenclass\MessageBroker\Producer\Drivers\LogProducer;
use Zenclass\MessageBroker\Tests\TestCase;

class ProducerTest extends TestCase
{
    /**
     * @dataProvider configOptionsAndExpectedProducerDriverClasses
     *
     * @throws BindingResolutionException
     */
    public function testThatApplicationCorrectlyUseProducerDriverConfigSettings(
        ?string $configOption,
        string $expectedClass
    ): void {
        $app = app();

        $app->config->set('message-broker.producer.default', $configOption);

        $producerDriver = $app->make(CanProduceMessageToTopic::class)->driver();

        $this->assertEquals($expectedClass, $producerDriver::class);
    }

    private function configOptionsAndExpectedProducerDriverClasses(): array
    {
        return [
            'log driver' => [
                'configOption' => self::LOG_DRIVER,
                'expectedClass' => LogProducer::class,
            ],

            'kafka-rest driver' => [
                'configOption' => self::KAFKA_REST_DRIVER,
                'expectedClass' => KafkaRestProducer::class,
            ],
        ];
    }
}
