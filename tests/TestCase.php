<?php

namespace Zenclass\MessageBroker\Tests;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected const LOG_DRIVER = 'log';
    protected const KAFKA_REST_DRIVER = 'kafka-rest';

    protected function getPackageProviders($app): array
    {
        return [
            'Zenclass\MessageBroker\MessageBrokerServiceProvider',
        ];
    }

    protected function usesKafkaRestProducerDriver($app): void
    {
        $app->config->set('message-broker.producer.default', self::KAFKA_REST_DRIVER);
        $app->config->set('message-broker.producer.drivers.kafka-rest.host', 'http://localhost:8082');
    }

    protected function usesLogProducerDriver($app): void
    {
        $app->config->set('message-broker.producer.default', self::LOG_DRIVER);
    }
}
